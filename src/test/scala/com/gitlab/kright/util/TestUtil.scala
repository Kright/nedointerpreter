package com.gitlab.kright.util

import org.junit.Assert.assertArrayEquals

object TestUtil:
  def toAnyRefArray[T](seq: Seq[T]): Array[AnyRef] = seq.map(_.asInstanceOf[AnyRef]).toArray

  def assertSeqEquals[T](expected: Seq[T], actual: Seq[T]): Unit =
    assertArrayEquals(s"expected = ${expected.mkString(",")}, actual = ${actual.mkString(",")}",
      toAnyRefArray(expected), toAnyRefArray(actual))
