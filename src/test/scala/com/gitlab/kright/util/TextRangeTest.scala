package com.gitlab.kright.util


import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.util.TextRange
import org.junit.Assert.{assertTrue, _}
import org.junit.Test


class TextRangeTest:
  @Test 
  def testIntersections(): Unit =
    assertIntersects(true, TextRange(0, 2), TextRange(1, 3))
    assertIntersects(false, TextRange(0, 2), TextRange(2, 4))
    assertIntersects(false, TextRange(0, 2), TextRange(3, 5))
    assertIntersects(true, TextRange(0, 2), TextRange(0, 2))
    assertIntersects(true, TextRange(0, 1), TextRange(0, 1))
    assertIntersects(true, TextRange(1, 2), TextRange(0, 3))

  def assertIntersects(hasIntersection: Boolean, a: TextRange, b: TextRange): Unit =
    assertEquals(hasIntersection, a hasIntersectionWith b)
    assertEquals(hasIntersection, b hasIntersectionWith a)
