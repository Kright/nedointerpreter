package com.gitlab.kright.interpreter

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.interpreter._
import com.gitlab.kright.optimizer.{Optimizer, UniqueId}
import com.gitlab.kright.parser.{given, _}
import com.gitlab.kright.scanner.{Identifier, Line, Scanner}
import com.gitlab.kright.util.TestUtil._
import com.gitlab.kright.util._
import org.junit.Assert.{assertEquals, _}
import org.junit.Test
import scala.util.chaining._
import scala.language.implicitConversions

import scala.collection.mutable.ArrayBuffer


class InterpreterStateTest:
  private def checkRaw(input: String,
                       check: (String, InterpreterState.Result) => Unit,                    
                       interpreter: InterpreterState): Unit =
    val (_, result) = interpreter.run(input)
    check(input, result)
  
  private def expect(input: String, expectedOutput: NedoValue): (String, InterpreterState.Result) => Unit = 
    (input, result) => result match {
      case Left(error) => fail(error.toString + s"\ninput = ${input}\nexpectedOutput = ${expectedOutput}\n")
      case Right(value) => assertEquals(expectedOutput, value)
    }
  
  private def shouldFail: (String, InterpreterState.Result) => Unit = 
    (input, result) => result match {
      case Left(error) => ()
      case Right(value) => fail(s"got ${value}, but expect error")
    }
  
  private def checkRaw(input: String, check: (String, InterpreterState.Result) => Unit, globalVariables: Map[String, NedoValue]): Unit =
    for (env <- Seq[EnvGC[Long, UniqueId, NedoValue]](
      PersistentEnvGC.withBuiltins(values = globalVariables)
    )) {
      checkRaw(input, check, InterpreterState(env = env))
      checkRaw(input, check, InterpreterState(env = env, optimizer = Optimizer()))
    }

  private def check(input: String, check: (String, InterpreterState.Result) => Unit, globalVariables: Map[String, NedoValue]): Unit = 
    require(!input.endsWith(";"))
    checkRaw(input, check, globalVariables)
    checkRaw(input + ";", check, globalVariables)

  private def check(input: String, expectedOutput: NedoValue, globalVariables: Map[String, NedoValue] = Map()): Unit =
    check(input, expect(input, expectedOutput), globalVariables)
    
  private def checkFail(input: String, globalVariables: Map[String, NedoValue] = Map()): Unit = 
    check(input, shouldFail, globalVariables)

  @Test
  def testNumbers(): Unit =
    check("42", 42.0)
    check("2 + 3", 5.0)
    check("2 + 3 * 4", 2.0 + 3 * 4)
    check("5 / 2", 2.5)
  
  @Test 
  def testNumberUnary(): Unit =
    check("-1", -1.0)
    check("--1", 1.0)

  @Test
  def testBoolean(): Unit =
    check("true", true)
    check("false", false)
  
  @Test 
  def testBooleanUnary(): Unit =
    check("!true", false)
    check("!false", true)
    check("!!true", true)
    check("!!false", false)

  @Test 
  def testBooleanBinary(): Unit =
    for
      first <- Seq(true, false)
      second <- Seq(true, false)
    do
      check(s"${first} == ${second}", expectedOutput = first == second)
      check(s"${first} != ${second}", expectedOutput = first != second)
      check(s"${first} or ${second}", expectedOutput = first || second)
      check(s"${first} and ${second}", expectedOutput = first && second)
  
  @Test
  def testBooleanLogic(): Unit = {
    for
      first <- Seq(false, true)
      second <- Seq(false, true)
    do
      check(s"$first and $second", first && second)
      check(s"$first or $second", first || second)
  }
      
  @Test     
  def testBooleanLaziness(): Unit =
    check("var a = 1; true and {a = 2; false}; a", 2.0)  
    check("var a = 1; false and {a = 2; false}; a", 1.0)  
    check("var a = 1; true or {a = 2; false}; a", 1.0)  
    check("var a = 1; false or {a = 2; false}; a", 2.0)

  @Test
  def testStrings(): Unit =
    check("\"str\"", "str")
    check(s"${"a".quoted} + ${"b".quoted}", "ab")
    check(s"${"a".quoted} < ${"b".quoted}", true)
    check(""""a" + "b"""", "ab")

  @Test
  def testSeveralInputs(): Unit =
    val state0 = InterpreterState()
    val (state1, Right(1.0)) = state0.run("1;")
    val (state2, Right(2.0)) = state1.run("2;")
    val expectedLines = Seq(Line("1;\n", TextRange(0, 3), 0), Line("2;\n", TextRange(3, 6), 1))
    assertSeqEquals(expectedLines, state2.previousLines.lines)

  @Test 
  def testSeveralStatemets(): Unit = 
    check("1 + 2; 3 + 4", 7.0)

  @Test
  def testVarDeclaration(): Unit =
    for (isMutable <- Seq(true, false)) {
      val code = if (isMutable) "var a = 2;" else "val a = 2;"
      val (s, t, block, resolvedBlock, optimizedBlock, r) = InterpreterState().runTransparent(code)
      val Right(Expr.Block(exprSeq, _)) = block
      assertEquals(exprSeq.size, 1)
      val decl = exprSeq(0)
      val Expr.Assignment(id, expr) = decl
      val SingleId("a", _, assignmentType) = id
      assertEquals(if (isMutable) AssignmentType.Var else AssignmentType.Val, assignmentType)
      val Expr.Literal(2.0, _) = expr
    }

  @Test
  def testIncorrectCode(): Unit =
    checkFail("!1.0")
    checkFail("""-"kek"""")
    checkFail("1 and 2")
    checkFail("1 + \"str\"")

  @Test 
  def testVarUsage(): Unit = 
    for(valWord <- Seq("val", "var")) {
      check(s"${valWord} a = 2; a", 2.0)
      check(s"${valWord} a = 2; ${valWord} b = 3; ${valWord} summ = a + b; summ", 5.0)
    }

  @Test 
  def testScoping(): Unit =
    check("{ 2 }", expectedOutput = 2.0)
    check("{ 2; }", expectedOutput = 2.0)
    check("val a = { 2 }; a + 1", expectedOutput = 3.0)
    check("val a = { val a = 1; a + a }; a", expectedOutput = 2.0)
    check("val b = 2; val a = { val a = 3; b + a }; a", expectedOutput = 5.0)
    check("var b = 2; val a = { val a = 3; b + a }; a", expectedOutput = 5.0)
    checkFail("{val a = 2}; a")

  @Test 
  def testAssignment(): Unit =
    check("var a = 2; a = a * 3; a", expectedOutput = 6.0)
    check("var a = 2; val b = { a = a + 2; a + 2 }; a", expectedOutput = 4.0)
    check("val a = 2; { var a = 4; a = a + 2.0; a }", expectedOutput = 6.0)
    check("var (a, b) = (1, 2); (a, b) = (b, a); (a, b)", expectedOutput = Seq(2.0, 1.0))
    check("var a = 1; (a, _) = (2, 3); a", expectedOutput = 2.0)
    check("var a = 1; (a, 3) = (2, 3); a", expectedOutput = 2.0)
    check("var (a, b, c) = (1, 2, 3); (a, (b, c)) = (4, (5, 6)); (a, b, c)", expectedOutput = Seq(4.0, 5.0, 6.0))
    checkFail("val (a, a) = (1, 2)")
  
  @Test 
  def testValReassignment(): Unit =
    checkFail("val a = 2; a = a * 3")

  @Test
  def testCortege(): Unit =
    check("()", Seq.empty)
    check("(1)", Seq(1.0))
    check("(1,)", Seq(1.0))
    check("(1, 2)", Seq(1.0, 2.0))
    check("(1, 2,)", Seq(1.0, 2.0))
    check("(2 + 3 * 4, 1.0)", Seq(2.0 + 3 * 4, 1.0))
    check("(2 + 3 * 4, 1.0,)", Seq(2.0 + 3 * 4, 1.0))
    check("((()), ())", Seq(Seq(Seq()), Seq()))
    check("val a = 2; (a, a+1)", Seq(2.0, 3.0))
    check("val a = 1; ({ val b = 2; b + a }, a, 0)", Seq(3.0, 1.0, 0.0))

  @Test 
  def testIf(): Unit = 
    check("if (true) 1.0 else 0.0", 1.0 )
    check("if (true) 1.0", 1.0 )
    check("if (false) 1.0 else 0.0", 0.0 )
    check("if (false) 1.0", null )
    check("val a = 1; val b = 2; if (true) a else b", 1.0 )
    check("val a = 1; val b = 2; if (false) a else b", 2.0 )
    check("if (true) { 1.0 + 2.0 } else 0.0", 3.0 )
    check("if (true) { 1.0 + 2.0 }", 3.0 )
    check("val a = 2; if (2.0 == a) { 1.0 + 2.0 }", 3.0 )
    check("var a = 2; if (true) { a = a + 2 }; a", 4.0 )
    check("if (true) if (false) 1.0 else 2.0", 2.0 ) 
    check("if (true) if (false) 1.0 else 2.0 else 3.0", 2.0 ) 
    check("if (false) 1.0 else if (false) 2.0 else 3.0", 3.0 )  

  @Test 
  def testWhile(): Unit =
    check("var a = 2; while(false) { a = 3}; a", 2.0)
    check("var a = 2; while(a == 2) { a = 3}; a", 3.0)
    check("var counter = 10; while (counter > 0) { counter = counter - 1}; counter", 0.0);

  @Test 
  def testTupleDeconstruction(): Unit = 
    check("val (a, b) = (1, 2); (b, a)", Seq(2.0, 1.0))
    check("val p = (1, 2); val (a, b) = p; (b, a)", Seq(2.0, 1.0))
    check("val (a, (b, c)) = (1.0, (2.0, 3.0)); (a, b, c)", Seq(1.0, 2.0, 3.0))
    check("val () = ()", null)
    check("var (a, b) = (1, 2); a = a + 10; b = b + 10; (a, b)", Seq(11.0, 12.0))
    check("var (a, (b, c)) = (1, (2, 3)); (a, b, c)", Seq(1.0, 2.0, 3.0))
    check("val (a, _) = (1, 2); a", 1.0)
    check("val (a, _, _) = (1, 2, 3); a", 1.0)
    check("val (a, _, _) = (1, 2, (3, (4, 5))); a", 1.0)    
    check("val _ = ()", null)
    check("val (a, true) = (1, true); a", 1.0)
    check("val (true) = (true)", null)
    check("val true = true", null)
    check("val null = null", null)
    check("val false = false", null)
    check("val 1.0 = 1.0", null)
    check("val 1 = 1", null)
    check("""val "s" = "s"""", null)
    check("""val (true, false, null, 1, "ab", a) = (1 == 1, 1 != 1, {}, 2 / 2, "a" + "b", 42); a""", 42.0)
    checkFail("val (a, b) = (1, 2, 3)")
    checkFail("val (a, b) = (1)")
  
  @Test 
  def testNestedModifiers(): Unit = {
    check("val (a, b) = (1, 2); (a, b)", Seq(1.0, 2.0))
    check("val (val a, val b) = (1, 2); (a, b)", Seq(1.0, 2.0))
    check("val (a, var b) = (1, 2); b = b + 3; (a, b)", Seq(1.0, 5.0))
  }

  @Test 
  def testFuncCall(): Unit =
    val variables = Seq(
      NedoFunc.pure("inc", value => Right(value.asDouble + 1)),
      NedoFunc.pure("addCarried", firstArg => Right(NedoFunc.pure("", secondArg => Right(firstArg.asDouble + secondArg.asDouble)))),
      NedoFunc.pure("swapPair", pair => Right(Seq(pair.asCortege(1), pair.asCortege(0)))),
    ).map(f => f.stringify -> f).toMap
    
    check("inc 1", 2.0, variables)
    check("addCarried 1 2", 3.0, variables)
    check("inc {1; 2}", 3.0, variables)
    check("swapPair (1, 2)", Seq(2.0, 1.0), variables)
    checkFail("notFound 1")