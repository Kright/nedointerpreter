package com.gitlab.kright.interpreter

import com.gitlab.kright.optimizer.{Optimizer, UniqueId}
import com.gitlab.kright.parser.Expr
import org.junit.Assert.{assertEquals, fail}
import org.junit.Test

class PerformanceTest:
  private def perfPersistent(code: String, expectedOutput: NedoValue, optimizer: Expr[UniqueId] => Expr[UniqueId] = identity): Unit =
    perf(code, expectedOutput, PersistentEnvGC.withBuiltins(), optimizer)

  private def perf(code: String, expectedOutput: NedoValue, environment: EnvGC[Long, UniqueId, NedoValue], optimizer: Expr[UniqueId] => Expr[UniqueId]): Unit =
    val (_, result) = InterpreterState(env = environment, optimizer = optimizer).run(code)
    result match
      case Left(error) => fail(error.toString)
      case Right(value) => assertEquals(value, expectedOutput)
  
  @Test(timeout = 3000)
  def testWhilePersistent(): Unit =
    perfPersistent(
      """var counter = 100000; 
        |while (counter > 0) { 
        |  counter = counter - 1
        |}; 
        |counter""".stripMargin, 0.0);

  @Test(timeout = 3000)
  def testWhilePersistentFast(): Unit =
    perfPersistent(
      """var counter = 100000; 
        |while (counter > 0) { 
        |  counter = counter - 1
        |}; 
        |counter""".stripMargin, 0.0, Optimizer());
