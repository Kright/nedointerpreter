package com.gitlab.kright.optimizer

import org.junit.Assert.assertEquals
import org.junit.Test
import com.gitlab.kright.parser.{given, _}

class EmptyBlockToNullTest:
  private def check(from: String, expected: String): Unit =
    def fix(code: String) = s"1.0; 2.0; ${code}"
    val realAst = emptyBlockToNull(toAst(fix(from)))
    val expectedAst = toAst(fix(expected))
    assertEquals(expectedAst.astString, realAst.astString)

  @Test 
  def simpleTest(): Unit = { 
    check("{}", "null")
    check("({})", "(null)")
    check("{1.0; {}; {{}}}", "{1.0; null; {null}}")
  }

  
