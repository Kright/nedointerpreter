package com.gitlab.kright.optimizer

import com.gitlab.kright.parser.{Expr, Parser}
import com.gitlab.kright.scanner.Scanner

def toAst(code: String): Expr[String] =
  val Right(tokens) = Scanner.scan(code, 0)
  val Right(expr) = Parser(tokens)
  expr