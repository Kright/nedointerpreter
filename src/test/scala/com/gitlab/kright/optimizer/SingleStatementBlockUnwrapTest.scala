package com.gitlab.kright.optimizer

import org.junit.Assert.assertEquals
import org.junit.Test
import com.gitlab.kright.parser.{given, _}

class SingleStatementBlockUnwrapTest:
  private def check(from: String, expected: String): Unit = 
    def fix(code: String) = s"1.0; 2.0; ${code}"
    val realAst = unwrapSingleStatementBlock(toAst(fix(from)))
    val expectedAst = toAst(fix(expected))
    assertEquals(expectedAst.astString, realAst.astString)
  
  @Test 
  def test(): Unit =  
    check("1.0", "1.0")
    check("{1.0}", "1.0")
    check("{{1.0}}", "1.0")
    check("{{{1.0}}}", "1.0")
    check("{{{1.0; 2.0}}}", "{1.0; 2.0}")
    check("{{{}}}", "{}")
    check("({{}}, {})", "({}, {})")
