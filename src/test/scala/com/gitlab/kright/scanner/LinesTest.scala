package com.gitlab.kright.scanner

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.util._
import org.junit.Assert._
import org.junit.Test

import com.gitlab.kright.scanner.Lines
import com.gitlab.kright.util.TestUtil._


class LinesTest:
  @Test 
  def testCreation(): Unit = 
    val code = "a\nbb\nccc\n\nd\n"
    val lines = Lines(code)
    lines.check()

    val expected = Array[Line](
      Line("a\n", TextRange(0, 2), 0), 
      Line("bb\n", TextRange(2, 5), 1), 
      Line("ccc\n", TextRange(5, 9), 2),
      Line("\n", TextRange(9, 10), 3), 
      Line("d\n", TextRange(10, 12), 4)
    )
    
    assertSeqEquals(expected, lines.lines)

  @Test
  def testCombineWithEmpty(): Unit =
    val lines = Lines("a\nbb\nccc\n\nd\n")
    lines.check()

    val empty = Lines()
    empty.check()

    assertSeqEquals(lines.lines, lines.combined(empty).lines)
    assertSeqEquals(lines.lines, empty.combined(lines).lines)
   
  @Test 
  def testCombine(): Unit =
    for 
      first <- Seq("\n", "\n\n", "a\n")
      second <- Seq("\n", "\n\n", "b\n") 
    do 
      checkCombine(first, second)
    
    
  def checkCombine(first: String, second: String): Unit =
    val firstLines = Lines(first)
    val secondLines = Lines(second)
    val combinedCodeLines = Lines(first + second)
    val combindLinesLines = firstLines.combined(secondLines)
  
    firstLines.check()
    secondLines.check()
    combinedCodeLines.check()
    combindLinesLines.check()
  
    assertSeqEquals(combindLinesLines.lines, combindLinesLines.lines)
    
  
  extension (lines: Lines) def check(): Unit =
    lines.lines.zipWithIndex.foreach{ (line, index) => assertEquals(index, line.number)}
    lines.lines.headOption.foreach(line => assertEquals(0, line.position.start))
    lines.lines.zip(lines.lines.drop(1)).foreach{ (prev, next) => assertEquals(prev.position.end, next.position.start)}
    assertEquals(lines.totalLength, lines.lines.map(_.text.size).sum)
    