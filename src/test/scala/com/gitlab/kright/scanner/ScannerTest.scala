package com.gitlab.kright.scanner

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.util._ 
import org.junit.Assert._
import org.junit.Test

import com.gitlab.kright.util.TestUtil._


class ScannerTest:
  @Test def testNumbers(): Unit =
    numbers.foreach{ (str, expected) =>
      val tokens = check(str, TokenType.Number, TokenType.Eof)
      val LexNumber(num) = tokens.head
      assertEquals(expected, num, 0.00001)
    }
    
  @Test def testKeywords(): Unit =
    keywords.foreach{ (name, tokenType) => 
      check(name, tokenType, TokenType.Eof)
    }
    
  @Test def testAllKeywords(): Unit = 
    val seq = keywords.toSeq
    val code = seq.map(_._1).mkString(" ")
    val tokenTypes = seq.map(_._2) :+ TokenType.Eof
    check(code, tokenTypes:_* )
  
  @Test def testIdentifier(): Unit =
    identifiers.foreach{ id => 
      val tokens = check(id, TokenType.Identifier, TokenType.Eof)
      assertEquals(id, tokens.head.lexeme.text)
    }
    
  @Test def testLexString(): Unit = 
    (identifiers ++ keywords.keys ++ numbers.keys :+ "").foreach{ s => 
      val tokens = check(s.quoted, TokenType.LexString, TokenType.Eof)
      val LexString(s2) = tokens.head
      assertEquals(s, s2)
    }
    
  @Test def testBinOp(): Unit = 
    for
      id <- identifiers
      (opS, opTok) <- operators ++ brackets
    do
      check(s"$id $opS $id", TokenType.Identifier, opTok, TokenType.Identifier, TokenType.Eof)
      if (!opS(0).isLetter) // not 'or' or 'and'
        check(s"$id$opS$id", TokenType.Identifier, opTok, TokenType.Identifier, TokenType.Eof)
      
      
  @Test def testComment(): Unit = {
    check("/* nothing */", TokenType.Eof)
    check("/*nothing*/", TokenType.Eof)
    check("//    ", TokenType.Eof)
    check("/* /* */ */", TokenType.Eof)
    check("/*/**/*/", TokenType.Eof)
    check("/*a/*b*/c*/", TokenType.Eof)
  }

  private val numbers = Map[String, Double](
    "1" -> 1.0,
    "9000" -> 9000.0,
    "12.34" -> 12.34,
  )
  
  private val operators = Map[String, TokenType](
    "+" -> TokenType.Plus, 
    "-" -> TokenType.Minus,
    "*" -> TokenType.Star, 
    "/" -> TokenType.Slash, 
    "!" -> TokenType.Bang, 
    "="  -> TokenType.Equal,
    "<" -> TokenType.Less,
    ">" -> TokenType.Greater,
    "==" -> TokenType.EqualEqual, 
    "!="  -> TokenType.BangEqual, 
    "<=" -> TokenType.LessEqual, 
    ">=" ->  TokenType.GreaterEqual, 
    "or" -> TokenType.Or, 
    "and" ->  TokenType.And,
    "." -> TokenType.Dot,
    ";" -> TokenType.Semicolon,
  )
  
  private val brackets = Map[String, TokenType](
    "(" -> TokenType.LeftParen,
    ")" -> TokenType.RightParen,
    "{" -> TokenType.LeftBrace,
    "}" -> TokenType.RightBrace,
  )
  
  private val identifiers = Seq("a", "word", "camelCase", "Capitalized", "withD1g1ts", "with_underscore", "_underscored")

  private val keywords = Map[String, TokenType](
    "and" -> TokenType.And,
    "class" -> TokenType.Class,
    "else" -> TokenType.Else,
    "false" -> TokenType.False,
    "def" -> TokenType.Def,
    "for" -> TokenType.For,
    "if" -> TokenType.If,
    "null" -> TokenType.Nil,
    "or" -> TokenType.Or,
    "return" -> TokenType.Return,
    "super" -> TokenType.Super,
    "this" -> TokenType.This,
    "true" -> TokenType.True,
    "var" -> TokenType.Var,
    "val" -> TokenType.Val,
    "while" -> TokenType.While,
  )

  private def check(code: String, expected: TokenType*): Seq[Token] = 
    Scanner.scan(code, 0) match 
      case Left(error) =>
        fail(s"can't parse '$code' error = ${error}")
        ??? 
      case Right(tokens) =>
        assertSeqEquals(expected, tokens.map(_.tokenType))
        tokens
