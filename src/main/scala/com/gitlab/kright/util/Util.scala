package com.gitlab.kright.util

import scala.collection.Factory
import scala.collection.mutable.ArrayBuffer

extension (s: String)
  def quoted = "\"" + s + "\""

extension[T] (seq: Seq[Option[T]])
  def transposed: Option[Seq[T]] = 
    val buf = new ArrayBuffer[T]()
    for (elem <- seq) {
      elem match {
        case Some(e) => buf += e
        case None => return None
      }
    }
    Option(buf.toSeq)

extension[L, R] (seq: Seq[Either[L, R]])
  def transposed: Either[L, Seq[R]] =
    val buf = new ArrayBuffer[R]()
    for (elem <- seq) {
      elem match {
        case Left(l) => return Left(l)
        case Right(e) => buf += e
      }
    }
    Right(buf.toSeq)

extension[L, R] (option: Option[Either[L, R]])
  def transposed: Either[L, Option[R]] =
    option match
      case None => Right(None)
      case Some(either) =>
        either match
          case Left(err) => Left(err)
          case Right(ok) => Right(Some(ok))
