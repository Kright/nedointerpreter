package com.gitlab.kright.util

case class TextRange(start: Int, end: Int) extends HasTextRange:
  require(start <= end)

  def size: Int = end - start

  def withOffset(offset: Int): TextRange = TextRange(start + offset, end + offset)
  
  def hasIntersectionWith(range: TextRange): Boolean = 
    this.start < range.end && this.end > range.start

  override def toString: String = s"${start}:${end}"

  override def textRange: TextRange = this


object TextRange:
  def inclusive(ranges: TextRange*): TextRange = 
    TextRange(ranges.map(_.start).min, ranges.map(_.end).max)

extension (s: String)
  def substring(range: TextRange): String = s.substring(range.start, range.end)

trait HasTextRange:
  def textRange: TextRange