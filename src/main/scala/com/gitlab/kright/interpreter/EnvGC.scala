package com.gitlab.kright.interpreter

import com.gitlab.kright.optimizer.UniqueId

trait EnvGC[Ptr, Id, V]:
  def heap: Heap[Ptr, V]
  def gcRoots: Map[Id, Ptr]
  
  def enterScope(scopeIds: Seq[Id]): EnvGC[Ptr, Id, V]
  def leaveScope(): Option[EnvGC[Ptr, Id, V]]
  def declareVariable(id: Id, value: V): Option[EnvGC[Ptr, Id, V]]
  def updateVarialbe(p: Ptr, value: V): Option[EnvGC[Ptr, Id, V]]
  def gc(getDeps: V => IterableOnce[Ptr]): EnvGC[Ptr, Id, V]

  // syntactic sugar
  inline def apply(id: Id) = gcRoots.get(id).map(heap(_))

  def allVariables: Map[Id, V] =
    gcRoots.flatMap{ (id, ptr) =>
      heap.get(ptr).map(v => id -> v)
    }