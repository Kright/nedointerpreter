package com.gitlab.kright.interpreter

object UnaryOperatorName:
  val prefix = "unary_"
  
  def apply(name: String): String = prefix + name 

  def unapply(name: String): Option[String] = 
    if (name.startsWith(prefix)) Option(name.substring(prefix.size)) else None