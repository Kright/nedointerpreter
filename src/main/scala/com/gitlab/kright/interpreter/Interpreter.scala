package com.gitlab.kright.interpreter

import com.gitlab.kright.{ErrorMessage, parser}
import com.gitlab.kright.parser.{Expr, Parser}
import com.gitlab.kright.scanner.{Scanner, Token, TokenType}

import scala.reflect.{ClassTag, ensureAccessible}
import scala.util.chaining._

class Interpreter(var state: InterpreterState = InterpreterState()):
  def run(source: String): InterpreterState.Result =
    runTransparent(source)._6

  def runTransparent(source: String) =
    state.runTransparent(source).tap { all =>
      state = all._1
    }
