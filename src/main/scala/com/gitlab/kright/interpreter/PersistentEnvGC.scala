package com.gitlab.kright.interpreter

import com.gitlab.kright.optimizer.UniqueId
import scala.collection.mutable


case class PersistentEnvGC[Ptr, Id, V](heap: Heap[Ptr, V],
                                       gcRoots: Map[Id, Ptr],
                                       scopes: List[Seq[Id]]) extends EnvGC[Ptr, Id, V]:
  override def enterScope(scopeIds: Seq[Id]): PersistentEnvGC[Ptr, Id, V] =
    require(scopeIds.forall(id => !gcRoots.contains(id)))
    copy(scopes = scopeIds :: scopes)

  override def leaveScope(): Option[PersistentEnvGC[Ptr, Id, V]] =
    scopes match
      case currentScope :: outerScopes =>
        Option(copy(
          gcRoots = gcRoots.removedAll(currentScope),
          outerScopes
        ))
      case Nil => None
  
  override def declareVariable(id: Id, value: V): Option[PersistentEnvGC[Ptr, Id, V]] =
    if (scopes.head.contains(id) && !gcRoots.contains(id)) 
      val (newHeap, ptr) = heap.added(value)
      Option(copy(
        heap = newHeap, 
        gcRoots = gcRoots.updated(id, ptr)
      ))
    else  
      None
      
  override def updateVarialbe(p: Ptr, value: V): Option[PersistentEnvGC[Ptr, Id, V]] =
    heap.update(p, value).map(h => copy(heap = h))

  override def gc(getDeps: V => IterableOnce[Ptr]): PersistentEnvGC[Ptr, Id, V] =
    val marked = new mutable.HashSet[Ptr]()
    val visited = new mutable.HashSet[Ptr]()
    
    marked ++= gcRoots.values
    
    while (!marked.isEmpty) {
      val current = marked.head
      visited += current
      for (dep <- getDeps(heap(current)) if !visited.contains(dep)) {
        marked += dep
      }
      marked -= current
    }
    
    copy(heap = heap.filter(ptr => !visited.contains(ptr)))

object PersistentEnvGC:
  def create(): PersistentEnvGC[Long, UniqueId, NedoValue] = 
    PersistentEnvGC(new HeapLong[NedoValue](), Map(), List())

  def create(variables: Map[UniqueId, NedoValue]): PersistentEnvGC[Long, UniqueId, NedoValue] =
    var env = create().enterScope(variables.keys.toSeq)
    variables.foreach { (id, v) =>
      env = env.declareVariable(id, v).get 
    }
    env
  
  def withBuiltins(values: Map[String, NedoValue] = Map()): PersistentEnvGC[Long, UniqueId, NedoValue] =
    val allValues: Map[String, NedoValue] = UnaryOperators.all ++ BinaryOperators.all ++ values
    val resolved: Map[String, UniqueId] = UniqueId.resolve(allValues.keys)
    val newValues: Map[UniqueId, NedoValue] = allValues.map{ (s, v) => resolved(s) -> v }
    PersistentEnvGC.create(newValues)