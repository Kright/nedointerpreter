package com.gitlab.kright.interpreter

trait Heap[K, V]:
  def added(v: V): (Heap[K, V], K)
  def apply(k: K): V
  def update(k: K, v: V): Option[Heap[K, V]]
  def get(k: K): Option[V]
  def filter(p: K => Boolean): Heap[K, V]

case class HeapLong[V](maxPtr: Long, map: Map[Long, V]) extends Heap[Long, V]:
  def this() = this(1, Map.empty)
  override def added(v: V): (HeapLong[V], Long) = (HeapLong(maxPtr + 1, map.updated(maxPtr, v)), maxPtr)
  override def apply(k: Long): V = map(k)
  override def update(k: Long, v: V): Option[Heap[Long, V]] = if (map.contains(k)) Option(copy(map = map.updated(k, v))) else None
  override def get(k: Long): Option[V] = map.get(k)
  override def filter(predicate: Long => Boolean): HeapLong[V] = HeapLong(maxPtr, map.filter((k, _) => predicate(k)))
