package com.gitlab.kright.interpreter

import com.gitlab.kright.interpreter.InterpreterState.eval
import com.gitlab.kright.{ErrorMessage, parser}
import com.gitlab.kright.parser.{given, _}
import com.gitlab.kright.interpreter.{NedoValue, stringify}
import com.gitlab.kright.optimizer.UniqueId
import com.gitlab.kright.optimizer.validateAssignments
import com.gitlab.kright.scanner.{Identifier, Lines, Scanner, Token, TokenType}
import com.gitlab.kright.util.TextRange

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.reflect.{ClassTag, classTag}


case class InterpreterState(previousLines: Lines = Lines(), 
                            env: EnvGC[Long, UniqueId, NedoValue] = PersistentEnvGC.withBuiltins(),
                            optimizer: Expr[UniqueId] => Expr[UniqueId] = identity):
  import InterpreterState.Result

  def run(source: String): (InterpreterState, Result) =
    val (state, _, _, _, _, result) = runTransparent(source)
    (state, result)

  def runTransparent(source: String): (
    InterpreterState, 
      Either[ErrorMessage, IndexedSeq[Token]], 
      Either[ErrorMessage, Expr[String]], 
      Either[ErrorMessage, Expr[UniqueId]], 
      Either[ErrorMessage, Expr[UniqueId]], 
      Result) = {
    val validSource = withLineEnd(source)
    val stateWithInput = this.copy(previousLines = previousLines.appended(validSource))

    val tokens = Scanner.scan(validSource, previousLines.totalLength)
    val tree = tokens.flatMap(Parser(_))
    val definedNames = env.allVariables.keys.map(n => n.name -> n).toMap
    val resolvedTree = tree.flatMap(UniqueId.resolve(definedNames, _))
    val optimizedTree = resolvedTree.map(optimizer)
    val validatedTree = optimizedTree.flatMap(validateAssignments(_, Set()))
    val result = validatedTree.flatMap(eval(env, _))
    val stateWithVariables = result.map{ case (env, _) => stateWithInput.copy(env = env) }.getOrElse(stateWithInput)

    (stateWithVariables, tokens, tree, resolvedTree, optimizedTree, result.map(_._2))
  }

  private def withLineEnd(s: String): String =
    if (!s.endsWith("\n"))
      s + "\n"
    else
      s

  override def toString: String =
    "InterpreterState:\n" + previousLines


object InterpreterState:
  type Result = Either[ErrorMessage, NedoValue]
  type Id = UniqueId
  type Ptr = Long
  type V = NedoValue

  object Result:
    def apply(error: ErrorMessage): Result = Left(error)
    def apply(value: NedoValue): Result = Right(value)
  
  private def eval(v: EnvGC[Ptr, Id, V], e: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
    e match
      case Expr.Cortege(exprs, _) => eval(v, exprs).map((env, arr) => (env, arr.toSeq))
      case Expr.Literal(obj, textRange) => Right((v, obj))
      case op@ Expr.Binary(left, operator, right) => 
        v(operator.id).getOrElse(BinaryOperators.unknownBinaryOperator).asFunc(eval, v, op)
      case b @ Expr.Block(exprs, _) => 
        eval(v.enterScope(b.blockDeclaredIds.toSeq), exprs).map((env, seq) => (env.leaveScope().get, seq.lastOption.getOrElse(null)))
      case Expr.Identifier(name, textRange) =>
        v(name) match
          case None => Left(ErrorMessage(s"variable ${name} wasn't declared", textRange))
          case Some(value) => Right((v, value))
      case Expr.Assignment(ids, expr) => evalAssignment(v, ids, expr)
      case Expr.IfThenElse(cond, thenExpr, elseExpr, _) => evalIfThenElse(v, cond, thenExpr, elseExpr)
      case Expr.WhileLoop(cond, body, _) => evalWhileLoop(v, cond, body)
      case f @ Expr.FuncCall(func, args) => evalFuncCall(v, func, args, f.textRange)
  
  private def evalFuncCall(env: EnvGC[Ptr, Id, V], 
                           funcExpr: Expr[Id], 
                           argsExpr: Expr[Id], 
                           range: TextRange): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
    (for
      funcWithEnv <- eval(env, funcExpr)
      func <- castToFunc(funcExpr, funcWithEnv.value)
      result <- func(eval, funcWithEnv.env, argsExpr)
    yield result).left.map(err => ErrorMessage(s"function call ${}", Some(range), Some(err)))

  private def castToFunc(expr: Expr[Id], func: NedoValue): Either[ErrorMessage, NedoFunc] =
    if (func.isFunc)
      Right(func.asFunc)
    else
      Left(ErrorMessage(s"object ${func.stringify} isn't a function", expr))
  
  private def evalAssignment(env: EnvGC[Ptr, Id, V],
                             pattern: LValueExpr[Id],
                             expr: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
    eval(env, expr).flatMap((env, value) => evalAssignment(env, pattern, value).map(env => (env, null)))
  
  private def evalAssignment[Id: HasAstString](env: EnvGC[Ptr, Id, V],
                                               pattern: LValueExpr[Id],
                                               value: NedoValue): Either[ErrorMessage, EnvGC[Ptr, Id, V]] = {
    pattern match {
      case WildCard(_) => Right(env)
      case SingleId(name, textRange, assignmentType) =>
        assignmentType match {
          case AssignmentType.Assignment => {
            env.gcRoots.get(name).flatMap { ptr =>
              env.updateVarialbe(ptr, value)
            }.toRight[ErrorMessage](ErrorMessage(s"variable ${name} wasn't updated", textRange))
          }
          case AssignmentType.Var | AssignmentType.Val => {
            val isMutable = assignmentType == AssignmentType.Var
            env.declareVariable(name, value)
              .toRight[ErrorMessage](ErrorMessage(s"can't declare ${name}", textRange)) 
          }
        }
      case ExpectedValue(expectedValue, textRange) =>
        if (value == expectedValue) 
          Right(env)
        else   
          Left(ErrorMessage(s"expected value ${expectedValue.stringify}, got ${value.stringify}", textRange))
      case Untupling(parts, textRange) =>
        if (!value.isCortege) {
          val s = s"can't deconstruct '${value}' to ${pattern.astString}"
          return Left(ErrorMessage(s, textRange))
        }
        val seq: Seq[NedoValue] = value.asCortege
        if (seq.size != parts.size) {
          return Left(ErrorMessage(s"can't deconstruct '${seq.size}' elements to ${parts.size}, '${value}' to ${pattern.astString}", textRange))
        }
        var v: EnvGC[Ptr, Id, V] = env
        parts.zip(seq).foreach { (id, rv) =>
          evalAssignment(v, id, rv.asInstanceOf[NedoValue]) match {
            case Left(err) => return Left(err)
            case Right(newEnv) => v = newEnv
          }
        }
        return Right(v)
    }
  }

  private def evalIfThenElse(v: EnvGC[Ptr, Id, V], cond: Expr[Id], thenExpr: Expr[Id], elseExpr: Option[Expr[Id]]) =
    evalBool(v, cond).flatMap{ (env, boolCond) =>
      boolCond match
        case true => eval(env, thenExpr)
        case false => elseExpr.map(eval(env, _)).getOrElse(Right(env, null))
    }
    
  private def evalBool(v: EnvGC[Ptr, Id, V], expr: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], Boolean)] =
    eval(v, expr).flatMap{ (env, v) =>
      if (v.isBoolean) 
        Right((env, v.asBoolean))
      else 
        Left(ErrorMessage(s"expected to be Boolean, get $v instead", expr))
    }
    
  @tailrec
  private def evalWhileLoop(v: EnvGC[Ptr, Id, V], cond: Expr[Id], body: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] = 
    evalBool(v, cond) match 
      case err @ Left(_) => err
      case Right((env, isTrue)) =>
        if (isTrue) {
          eval(env, body) match
            case err @ Left(_) => err
            case Right(bodyEnv, _) => evalWhileLoop(bodyEnv, cond, body)
        } else 
          Right(env, null)
  
  private def eval(v: EnvGC[Ptr, Id, V], exprs: Seq[Expr[Id]]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], ArrayBuffer[NedoValue])] = 
    val results = new ArrayBuffer[NedoValue]()
    var currentEnv = v 
    exprs.foreach{ expr => 
      eval(currentEnv, expr) match
        case Left(err) => 
          return Left(err)
        case Right(env, value) => 
          currentEnv = env 
          results += value 
    }
    Right((currentEnv, results))

extension[Ptr, Id, V] (pair: (EnvGC[Ptr, Id, V], _)) private def env: EnvGC[Ptr, Id, V] = pair._1
extension (pair: (_, NedoValue)) private def value: NedoValue = pair._2