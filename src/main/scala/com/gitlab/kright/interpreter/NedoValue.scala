package com.gitlab.kright.interpreter

import com.gitlab.kright.util._

type NedoValue = Null | Boolean | Double | String | NedoFunc | Seq[_] 

extension (v: NedoValue)
  def asDouble: Double = v.asInstanceOf[Double]
  def isDouble: Boolean = v.isInstanceOf[Double]

  def asBoolean: Boolean = v.asInstanceOf[Boolean]
  def isBoolean: Boolean = v.isInstanceOf[Boolean]
  
  def asString: String = v.asInstanceOf[String]
  def isString: Boolean = v.isInstanceOf[String]
  
  def asCortege: Seq[NedoValue] = v.asInstanceOf[Seq[NedoValue]]
  def isCortege: Boolean = v.isInstanceOf[Seq[_]]
  
  def asFunc: NedoFunc = v.asInstanceOf[NedoFunc]
  def isFunc: Boolean = v.isInstanceOf[NedoFunc]

  def isEqual(b: NedoValue): Boolean = 
    if (v == null) { b == null }
    else v == b 

  def stringify: String =
    v match
      case null => "null"
      case b: Boolean => b.toString
      case s: String => s.quoted
      case d: Double => 
        val s = d.toString
        if (s.endsWith(".0")) s.substring(0, s.length - 2) else s
      case f: NedoFunc => f.stringify
      case _: Seq[_] => 
        v.asCortege.map(_.stringify).mkString("(", ", ", ")")
