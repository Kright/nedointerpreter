package com.gitlab.kright.interpreter

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.parser.Expr

trait NedoFunc:
  def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                        env: EnvGC[Ptr, Id, V],
                        args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)]

  def stringify: String

object NedoFunc:
  def pure(name: String, f: NedoValue => Either[ErrorMessage, NedoValue]): NedoFunc =
    new NedoFunc :
      override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                     env: EnvGC[Ptr, Id, V],
                                     args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
        evalFunc(env, args).flatMap { (newEnv, nedoValue) =>
          f(nedoValue).map(result => (newEnv, result))
        }

      override def stringify: String = name
