package com.gitlab.kright.interpreter

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.interpreter.InterpreterState.{Result, eval, evalBool}
import com.gitlab.kright.parser.Expr

object BinaryOperators:
  val all: Map[String, NedoFunc] = Seq[NedoFunc](
    logicalAnd,
    logicalOr,
    opDouble("-", _ - _),
    opDouble("/", _ / _),
    opDouble("*", _ * _),
    opOp("+", _ + _, _ + _),
    opOp(">", _ > _, _ > _),
    opOp("<", _ < _, _ < _),
    opOp(">=", _ >= _, _ >= _),
    opOp("<=", _ <= _, _ <= _),
    pureDirectOp("!=") { (left, right) => Right(!left.isEqual(right)) },
    pureDirectOp("==") { (left, right) => Right(left.isEqual(right)) },
  ).map(f => f.stringify -> f).toMap

  val unknownBinaryOperator = new NedoFunc {
    override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                   env: EnvGC[Ptr, Id, V],
                                   args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
      Left(ErrorMessage("unknown binary operator", args))

    override def stringify: String = ???
  }

  private def logicalAnd = new NedoFunc {
    override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                   env: EnvGC[Ptr, Id, V],
                                   args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
      val Expr.Binary(left, _, right) = args
      evalBool(evalFunc, env, left).flatMap { (env, value) =>
        value match
          case true => evalBool(evalFunc, env, right)
          case false => Right((env, false))
      }

    override def stringify: String = "and"
  }

  private def logicalOr = new NedoFunc {
    override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                   env: EnvGC[Ptr, Id, V],
                                   args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
      val Expr.Binary(left, _, right) = args
      evalBool(evalFunc, env, left).flatMap { (newEnv, value) =>
        value match
          case true => Right(newEnv, true)
          case false => evalBool(evalFunc, newEnv, right)
      }

    override def stringify: String = "or"
  }

  private def evalBool[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                   env: EnvGC[Ptr, Id, V],
                                   expr: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], Boolean)] =
    evalFunc(env, expr).flatMap { (newEnv, v) =>
      if (v.isBoolean)
        Right((newEnv, v.asBoolean))
      else
        Left(ErrorMessage(s"expected to be Boolean, get $v instead", expr))
    }

  private def opDouble(name: String, op: (Double, Double) => NedoValue): NedoFunc =
    pureDirectOp(name) { (leftV, rightV) =>
      if (leftV.isDouble && rightV.isDouble) Result(op(leftV.asInstanceOf[Double], rightV.asInstanceOf[Double]))
      else Result(ErrorMessage(s"can't apply ${name} for ${leftV.stringify} and ${rightV.stringify}, expected Doubles"))
    }

  private def opString(name: String, op: (String, String) => NedoValue): NedoFunc =
    pureDirectOp(name) { (leftV, rightV) =>
      if (leftV.isString && rightV.isString) Result(op(leftV.asInstanceOf[String], rightV.asInstanceOf[String]))
      else Result(ErrorMessage(s"can't apply ${name} for ${leftV.stringify} and ${rightV.stringify}, expected Strings"))
    }

  private def opOp(name: String, op1: (Double, Double) => NedoValue, op2: (String, String) => NedoValue): NedoFunc =
    pureDirectOp(name) { (leftV, rightV) =>
      if (leftV.isDouble && rightV.isDouble) Result(op1(leftV.asInstanceOf[Double], rightV.asInstanceOf[Double]))
      else if (leftV.isString && rightV.isString) Result(op2(leftV.asInstanceOf[String], rightV.asInstanceOf[String]))
      else Result(ErrorMessage(s"can't apply ${name} for ${leftV.stringify} and ${rightV.stringify}, expected both Strings or both Doubles"))
    }

  private def pureDirectOp(name: String)(op: (NedoValue, NedoValue) => Either[ErrorMessage, NedoValue]): NedoFunc =
    new NedoFunc {
      override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                     env: EnvGC[Ptr, Id, V],
                                     args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] = {
        val Expr.Binary(left, _, right) = args
        (for {
          leftV <- evalFunc(env, left)
          rightV <- evalFunc(leftV.env, right)
          result <- op(leftV.value, rightV.value)
        } yield (rightV.env, result))
          .left.map(err => err.copy(textRange = Option(args.textRange)))
      }

      override def stringify: String = name
    }  