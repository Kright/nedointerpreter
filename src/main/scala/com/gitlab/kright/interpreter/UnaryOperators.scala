package com.gitlab.kright.interpreter

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.parser.Expr
import com.gitlab.kright.util.quoted

object UnaryOperators:
  val logicNot: NedoFunc = NedoFunc.pure(UnaryOperatorName("!"), v => {
    if (v.isBoolean) Right(!v.asBoolean)
    else Left(ErrorMessage(s"expected Boolean for '!', got ${v.stringify}"))
  })

  val minus: NedoFunc = NedoFunc.pure(UnaryOperatorName("-"), v => {
    if (v.isDouble) Right(-v.asDouble)
    else Left(ErrorMessage(s"expected Double for '-', got ${v.stringify}"))
  })

  val all: Map[String, NedoFunc] = Seq(logicNot, minus).map(f => f.stringify -> f).toMap

  val unknownUnaryOperator = new NedoFunc {
    override def apply[Ptr, Id, V](evalFunc: (EnvGC[Ptr, Id, V], Expr[Id]) => Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)],
                                   env: EnvGC[Ptr, Id, V],
                                   args: Expr[Id]): Either[ErrorMessage, (EnvGC[Ptr, Id, V], NedoValue)] =
      Left(ErrorMessage("unknown unary operator", args))

    override def stringify: String = ???
  }