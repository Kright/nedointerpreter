package com.gitlab.kright

import com.gitlab.kright.parser.Expr
import com.gitlab.kright.scanner.{Lexeme, Line, Token}
import com.gitlab.kright.util.{HasTextRange, TextRange}

private def repeat(s: String, count: Int) =
  (0 until count).map(_ => s).mkString("")

def formatError(errorMsg: String, line: String, textRange: TextRange): String =
  errorMsg + "\n" + line + "\n" + repeat(" ", textRange.start) + repeat("^", textRange.size) + "--Here!"

def formatError(errorMsg: String, line: Line, textRange: TextRange): String =
  val prefix = s"${line.number} | "
  formatError(errorMsg, prefix + line, textRange.withOffset(prefix.size))


case class ErrorMessage(msg: String, textRange: Option[TextRange] = None, underlying: Option[ErrorMessage] = None):
  def withOffset(offset: Int): ErrorMessage =
    copy(
      textRange = textRange.map(_.withOffset(offset)),
      underlying = underlying.map(_.withOffset(offset))
    )
    
  override def toString: String = 
    s"$msg${textRange.map(r => s" at ${r}")}${underlying.map(u => s"\n$u").getOrElse("")}"


object ErrorMessage:
  def apply(msg: String, textRange: HasTextRange): ErrorMessage = 
    ErrorMessage(msg, Some(textRange.textRange))
    
  def apply(msg: String, underlying: ErrorMessage): ErrorMessage = 
    ErrorMessage(msg, None, Some(underlying))
