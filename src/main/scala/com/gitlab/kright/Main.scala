package com.gitlab.kright

import com.gitlab.kright.parser.{Expr, Parser}
import com.gitlab.kright.scanner.{Scanner, Token}
import com.gitlab.kright.interpreter.{Interpreter, InterpreterState, NedoValue, stringify}

import scala.io.StdIn.readLine

object Main:
  
  def main(args: Array[String]): Unit =
    args.size match
      case 0 => runPrompt()
      case 1 => runFile(args(0))
      case _ => println(s"wrong args = ${args.mkString(",")}")

  def runPrompt(): Unit = 
    val interpreter = Interpreter()
    
    while true do 
      print("> ")
      val line = readLine()
      if (line == null) {
        println("exit!")
        return
      }
      val (newState, tokens, expr, resolvedExpr, optimizedExpr, result) = interpreter.runTransparent(line)
      expr.foreach(e => println(s"parsed = ${e}") )
      result match
        case Left(error) => println(s"error = $error")
        case Right(value) => println(s"evaluated = ${value.stringify}")
      println(s"env = ${newState.env}")
        
  def runFile(name: String) =
    println(s"runFile($name)")

  def msg = "I was compiled by dotty :)"

