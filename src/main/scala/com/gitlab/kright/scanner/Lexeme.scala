package com.gitlab.kright.scanner

import com.gitlab.kright.scanner.Lexeme
import com.gitlab.kright.util._ 

case class Lexeme(text: String, override val textRange: TextRange) extends HasTextRange:
  override def toString: String = s"${text.quoted} at ${textRange}"
