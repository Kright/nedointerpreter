package com.gitlab.kright.scanner

import scala.reflect.ClassTag
import com.gitlab.kright.interpreter.NedoValue
import com.gitlab.kright.util.{HasTextRange, TextRange}

case class Token(tokenType: TokenType, lexeme: Lexeme, literalObject: Option[NedoValue] = None) extends HasTextRange:
  override def textRange: TextRange = lexeme.textRange
  
  override def toString: String =
    val objStr = literalObject.map(o => s", ${o}").getOrElse("")
    s"Token(${tokenType}, ${lexeme}${objStr})"


private def getLiteral[T : ClassTag](token: Token, tokenType: TokenType): Option[T] =
  if (token.tokenType == tokenType)
    token.literalObject.map(_.asInstanceOf[T])
  else
    None


object Identifier:
  def unapply(t: Token): Option[String] = getLiteral[String](t, TokenType.Identifier)

object LexString:
  def apply(content: String, lexeme: Lexeme): Token = 
    Token(TokenType.LexString, lexeme, Option(content))
  
  def unapply(t: Token): Option[String] = getLiteral[String](t, TokenType.LexString)

object LexNumber:
  def unapply(t: Token): Option[Double] = getLiteral[Double](t, TokenType.Number)
