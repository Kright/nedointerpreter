package com.gitlab.kright.scanner

import com.gitlab.kright._
import com.gitlab.kright.util._
import Scanner._

import scala.collection.mutable.ArrayBuffer
import scala.language.{implicitConversions, postfixOps}
import scala.util.chaining._


private def getLineEnd(start: Int, str: String): Int = 
  val index = str.indexOf('\n', start)
  if (index != -1)
    math.min(index + 1, str.size)
  else
    str.size 
    

private class Scanner(source: String):
  private var currentPos: Int = 0
  
  private def isAtEnd(): Boolean =
    currentPos >= source.size

  private def advance(): Char =
    val current = source.charAt(currentPos)
    currentPos += 1 
    current
  
  private def advance(count: Int): Unit =
    (1 to count).foreach(_ => advance())
  
  private def nextChar: Option[Char] = 
    peekChar(0)

  private def peekChar(offset: Int): Option[Char] = 
    source.lift(currentPos + offset)
  
  private def `match`(expected: Char): Boolean =
    val matched = nextChar.contains(expected)
    if (matched)
      advance()
    matched

  private def makeLexeme(start: Int, end: Int) = 
    val range = TextRange(start, end)
    Lexeme(source.substring(range), range)
  
  private def scanIdentifier(): Token =
    val start = currentPos - 1
    while nextChar.exists(c => c.isLetterOrDigit || c == '_' ) do
      advance()
    val lexeme = makeLexeme(start, currentPos)
    keywords.get(lexeme.text) match
      case Some(keyword) => scanner.Token(keyword, lexeme)
      case None => scanner.Token(TokenType.Identifier, lexeme, Option(lexeme.text))

  private def scanNumber(): Token =
    val start = currentPos - 1
    while nextChar.exists(_.isDigit) do 
      advance()
    
    if (peekChar(0).contains('.') && peekChar(1).exists(_.isDigit)) {
      advance()
      while nextChar.exists(_.isDigit) do
        advance()
    }
    
    val lexeme = makeLexeme(start, currentPos)
    scanner.Token(TokenType.Number, lexeme, Option(lexeme.text.toDouble))
  
  private def nextPair: Option[String] =
    for
      ch1 <- peekChar(0)
      ch2 <- peekChar(1) 
    yield s"$ch1$ch2"
  
  private def consumeSlashStarComment(): Unit = 
    while true do
      nextPair match
        case Some("/*") => 
          advance(2)
          consumeSlashStarComment()
        case Some("*/") => 
          advance(2)
          return
        case None => 
          advance()
          return
        case _ => advance()
        
  private def scanLexString(): Token | ErrorMessage = 
    val start = currentPos - 1
    val string = StringBuilder()
    while !isAtEnd() do
      val next = advance()
      if (next == '\"')
        return LexString(string.toString, makeLexeme(start, currentPos))
      string += next

    return ErrorMessage("can't parse string, end of file", TextRange(start, source.size))

  private def scanToken(): Token | ErrorMessage =
    if (isAtEnd())
      return scanner.Token(TokenType.Eof, makeLexeme(source.size, source.size))
    
    val char = advance()
    
    val ch2Token: Option[Token] = for 
      secondChar <- nextChar
      tokenType <- twoCharTokens.get(s"$char$secondChar")
    yield scanner.Token(tokenType, makeLexeme(currentPos - 1, currentPos + 1)).tap(_ => advance())
    
    if (ch2Token.exists(_.tokenType == TokenType.SlashSlash)) {
      while nextChar.exists(_ != '\n') do
        advance()
      return scanToken()
    }
    
    if (ch2Token.exists(_.tokenType == TokenType.SlashStar)) {
      consumeSlashStarComment()
      return scanToken()
    }

    val token: Option[Token] = ch2Token.orElse(
      singleCharTokens.get(char).map { tokenType =>
        scanner.Token(tokenType, makeLexeme(currentPos - 1, currentPos))
      }
    )
    
    token.foreach(return _)
      
    if (whitespaceChars.contains(char))
      return scanToken()
    
    if (char.isLetter || char == '_')
      return scanIdentifier()
      
    if (char.isDigit)
      return scanNumber()
      
    if (char == '\"')
      return scanLexString()

    return ErrorMessage(s"unknown symbol '$char'", TextRange(currentPos - 1, currentPos))

  def scanTokens(): Either[ErrorMessage, IndexedSeq[Token]] =
    val tokens = new ArrayBuffer[Token]()
    while !tokens.lastOption.exists(_.tokenType == TokenType.Eof) do
      scanToken() match
        case t: Token => tokens += t
        case error: ErrorMessage => return Left(error)
    Right(tokens.toIndexedSeq)


object Scanner:
  def scan(source: String, offset: Int): Either[ErrorMessage, IndexedSeq[Token]] = 
    Scanner(source).scanTokens().addOffset(offset)
    
  extension (result: Either[ErrorMessage, IndexedSeq[Token]])
    private def addOffset(offset: Int): Either[ErrorMessage, IndexedSeq[Token]] = 
      result match 
        case Left(error) => Left(error.withOffset(offset))
        case Right(tokens) => Right(tokens.map{ t =>
          t.copy(lexeme = t.lexeme.copy(textRange = t.lexeme.textRange.withOffset(offset)))
        })

  private val singleCharTokens: Map[Char, TokenType] = Map(
    '(' -> TokenType.LeftParen,
    ')' -> TokenType.RightParen,
    '{' -> TokenType.LeftBrace,
    '}' -> TokenType.RightBrace,
    ',' -> TokenType.Comma,
    '.' -> TokenType.Dot,
    '-' -> TokenType.Minus,
    '+' -> TokenType.Plus,
    ';' -> TokenType.Semicolon,
    '*' -> TokenType.Star,
    '!' -> TokenType.Bang,
    '=' -> TokenType.Equal,
    '<' -> TokenType.Less,
    '>' -> TokenType.Greater,
    '/' -> TokenType.Slash,
  )
  
  private val twoCharTokens: Map[String, TokenType] = Map(
    "!=" -> TokenType.BangEqual,
    "==" -> TokenType.EqualEqual,
    "<=" -> TokenType.LessEqual,
    ">=" -> TokenType.GreaterEqual,
    "//" -> TokenType.SlashSlash, 
    "/*" -> TokenType.SlashStar,
  )
  
  private val whitespaceChars: Set[Char] = Set(' ', '\t', '\r', '\n')

  private val keywords: Map[String, TokenType] = Map(
    "and" -> TokenType.And,
    "class" -> TokenType.Class,
    "else" -> TokenType.Else,
    "false" -> TokenType.False,
    "for" -> TokenType.For,
    "def" -> TokenType.Def,
    "if" -> TokenType.If,
    "null" -> TokenType.Nil,
    "or" -> TokenType.Or,
    "return" -> TokenType.Return,
    "super" -> TokenType.Super,
    "this" -> TokenType.This,
    "true" -> TokenType.True,
    "var" -> TokenType.Var,
    "val" -> TokenType.Val,
    "while" -> TokenType.While,
  )
