package com.gitlab.kright.scanner

import com.gitlab.kright.util.TextRange
import com.gitlab.kright.util._ 

import scala.collection.mutable.ArrayBuffer
import scala.language.implicitConversions

case class Lines(lines: IndexedSeq[Line] = IndexedSeq()):
  def appended(code: String) = 
    this.combined(Lines(code))

  def combined(another: Lines): Lines =
    Lines(lines ++ another.shifted(totalLength, lines.size).lines)

  def totalLength: Int = 
    lines.lastOption.map(_.position.end).getOrElse(0)

  def getByRange(range: TextRange): Seq[Line] =
    lines.filter(_.position.hasIntersectionWith(range))

  private def shifted(rangeOffset: Int, lineNoOffset: Int): Lines = 
    Lines(lines.map(line => line.copy(
      number = line.number + lineNoOffset, 
      position = line.position.withOffset(rangeOffset))
    ))

  override def toString: String = 
    lines.mkString("Lines:\n", "", "")

    

object Lines:
  def apply(code: String): Lines = 
    require(code.endsWith("\n"))
    Lines(code.split("\n").map(_ + "\n"))

  private def apply(lines: Seq[String]): Lines = 
    val result = new ArrayBuffer[Line]()
    var offset = 0
    for ((line, lineNo) <- lines.zipWithIndex) {
      require(line.nonEmpty && line.indexOf('\n') == line.size - 1)
      result += Line(line, TextRange(offset, offset + line.size), lineNo)
      offset += line.size
    } 
    Lines(result.toIndexedSeq)
