package com.gitlab.kright.scanner

import com.gitlab.kright.util._

import scala.math.Ordering

case class Line(text: String, position: TextRange, val number: Int):
  require(position.size == text.size)

  override def toString: String =
    s"${number} : ${text.quoted}"