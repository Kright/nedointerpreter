package com.gitlab.kright.scanner

import scala.reflect.ClassTag


enum TokenType:
  case 
    // single-character tokens 
    LeftParen, RightParen, // () 
    LeftBrace, RightBrace, // {}
    Comma, Dot, Minus, Plus, Semicolon, Slash, Star,
    // one or two character tokens
    Bang, BangEqual,
    Equal, EqualEqual,
    Greater, GreaterEqual,
    Less, LessEqual, 
    // comments
    SlashSlash, SlashStar, StarSlash,
    // literals
    Identifier, LexString, Number,
    // keywords
    And, Class, Else, False, Def, For, If, Nil, Or, 
    Return, Super, This, True, Var, Val, While,
    Eof
