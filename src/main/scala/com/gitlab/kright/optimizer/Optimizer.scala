package com.gitlab.kright.optimizer

import com.gitlab.kright.parser.Expr
import com.gitlab.kright.parser.Expr.{Assignment, Literal}

object Optimizer:
  def apply[T](): Expr[T] => Expr[T] =
    emptyBlockToNull[T]
      .andThen(unwrapSingleStatementBlock[T])

def unwrapSingleStatementBlock[T](expr: Expr[T]): Expr[T] = Expr.transform(expr, {
  case Expr.Block(exprs, textRange) if (exprs.size == 1 && exprs.head.declaredIds.isEmpty) => exprs.head
  case e => e
})

def emptyBlockToNull[T](expr: Expr[T]): Expr[T] =
  Expr.transform(expr, {
    case Expr.Block(exprs, textRange) if exprs.isEmpty => Literal(null, textRange)
    case e => e
  })
