package com.gitlab.kright.optimizer

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.interpreter.UnaryOperators
import com.gitlab.kright.parser.{LValueExpr, ExpectedValue, Expr, HasAstString, SingleId, Untupling, WildCard}
import com.gitlab.kright.parser.Expr.Identifier
import com.gitlab.kright.util.TextRange
import com.gitlab.kright.util._

// todo constant propagation

case class UniqueId(val uniqueId: Int, val name: String):
  override def equals(obj: Any): Boolean =
    obj match
      case UniqueId(id, _) => uniqueId == id
      case _ => false

  override def hashCode(): Int = 
    uniqueId.##

object UniqueId:
  def resolve(builtins: Map[String, UniqueId], expr: Expr[String]): Either[ErrorMessage, Expr[UniqueId]] =
    val idFactory = UniqueIdFactory(counter = builtins.map(_._2.uniqueId).maxOption.getOrElse(0))
    resolve(expr, Scope(builtins, None))(idFactory)

  def resolve(names: Iterable[String]): Map[String, UniqueId] =
    val idFactory = UniqueIdFactory()
    names.toSeq.distinct.map{ (name) => name -> idFactory(name) }.toMap

  private def resolve(expr: Expr[String], scope: Scope)
                     (implicit idFactory: UniqueIdFactory): Either[ErrorMessage, Expr[UniqueId]] = 
    expr match
      case b @ Expr.Binary(left, operator, right) => 
        for {
          rl <- resolve(left, scope)
          rop <- resolve(operator, scope)
          rr <- resolve(right, scope)
        } yield Expr.Binary(rl, rop, rr)
      case c @ Expr.Cortege(exprs, textRange) =>
        for {
          newExprs <- exprs.map(e => resolve(e, scope)).transposed
        } yield Expr.Cortege(newExprs, textRange)
      case b @ Expr.Block(exprs, textRange) =>
        val blockScope = Scope(b.blockDeclaredIds.toSeq.map(id => id -> idFactory(id)).toMap, Some(scope))
        for {
          newExprs <- exprs.map(e => resolve(e, blockScope)).transposed 
        } yield Expr.Block(newExprs, textRange)
      case Expr.Assignment(ids, expr) => 
        for {
          newIds <- resolve(ids, scope)
          newExpr <- resolve(expr, scope)
        } yield Expr.Assignment(newIds, newExpr)
      case i @ Expr.Identifier(id, textRange) => resolve(i, scope)
      case l @ Expr.Literal(value, textRange) => Right(l)
      case ite @ Expr.IfThenElse(condition, thenExpr, elseExpr, textRange) => 
        for {
          newCondition <- resolve(condition, scope)
          newThenExpr <- resolve(thenExpr, scope)
          newElseExpr <- elseExpr.map(resolve(_, scope)).transposed
        } yield Expr.IfThenElse(newCondition, newThenExpr, newElseExpr, textRange)
      case wl @ Expr.WhileLoop(condition, body, textRange) => 
        for {
          newCondition <- resolve(condition, scope)
          newBody <- resolve(body, scope)
        } yield Expr.WhileLoop(newCondition, newBody, textRange)
      case f @ Expr.FuncCall(func, arg) => 
        for {
          newFunc <- resolve(func, scope)
          newArg <- resolve(arg, scope)
        } yield Expr.FuncCall(newFunc, newArg)
  
  private def resolve(ids: LValueExpr[String], scope: Scope): Either[ErrorMessage, LValueExpr[UniqueId]] =
    ids match {
      case SingleId(id, textRange, assignmentType) => resolve(id, scope, textRange).map(SingleId(_ , textRange, assignmentType))
      case Untupling(parts, textRange) => parts.map(resolve(_, scope)).transposed.map(Untupling(_, textRange))
      case WildCard(textRange) => Right(WildCard(textRange))
      case ExpectedValue(value, textRange) => Right(ExpectedValue(value, textRange))
    }
  
  private def resolve(id: Identifier[String], scope: Scope): Either[ErrorMessage, Identifier[UniqueId]] =
    resolve(id.id, scope, id.textRange).map(Identifier(_, id.textRange))

  private def resolve(id: String, scope: Scope, textRange: TextRange): Either[ErrorMessage, UniqueId] = 
    scope(id).toRight(ErrorMessage(s"unknown id: ${id}", textRange))
    
  given HasAstString[UniqueId] with
    extension (id: UniqueId) 
      def astString: String = id.name

private class Scope(val declarations: Map[String, UniqueId], parentScope: Option[Scope]):
  def apply(id: String): Option[UniqueId] = declarations.get(id).orElse(parentScope.flatMap(p => p(id)))

private class UniqueIdFactory(var counter: Int = 0):
  def apply(name: String): UniqueId = 
    counter += 1
    UniqueId(counter, name)
