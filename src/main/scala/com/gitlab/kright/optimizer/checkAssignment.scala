package com.gitlab.kright.optimizer

import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.parser._
import com.gitlab.kright.util.TextRange

def validateAssignments(expr: Expr[UniqueId], existingValues: Set[UniqueId]): Either[ErrorMessage, Expr[UniqueId]] =
  checkAssignment(expr, existingValues) match 
    case Some(err) => Left(err)
    case None => Right(expr)

private def checkAssignment[Id](expr: Expr[Id], existingValues: Set[Id]): Option[ErrorMessage] =
  expr match
    case Expr.Binary(left, _, right) => checkAssignment(Seq(left, right), existingValues)
    case Expr.Cortege(exprs, _) => checkAssignment(exprs, existingValues)
    case Expr.Block(exprs, textRange) => 
      val ids: Seq[Id] = exprs.collect {
        case Expr.Assignment(ids, expr) => declaredVals(ids)
      }.flatten
      checkUnique(ids, expr.textRange).orElse{
        checkAssignment(exprs, existingValues ++ ids)
      }
    case Expr.Assignment(ids, expr) => {
      checkUnique(ids.declaredIds, expr.textRange)
      val doubleAssignment: Set[Id] = existingValues.intersect(assignedIds(ids).toSet)
      if (doubleAssignment.nonEmpty) {
        Option(ErrorMessage(s"double assigned values = ${doubleAssignment.mkString("[", ", ", "]")}", expr.textRange))
      } else {
        None 
      }
    } 
    case Expr.Identifier(_, _) => None
    case Expr.Literal(_, _) => None
    case Expr.IfThenElse(condition, thenExpr, elseExpr, _) => 
      checkAssignment(Seq(condition, thenExpr) ++ elseExpr.map(Seq(_)).getOrElse(Seq()), existingValues)
    case Expr.WhileLoop(condition, body, _) => checkAssignment(Seq(condition, body), existingValues)
    case Expr.FuncCall(func, arg) => checkAssignment(arg, existingValues)

private def checkAssignment[Id](exprs: Seq[Expr[Id]], existingValues: Set[Id]): Option[ErrorMessage] = 
  exprs.view.map(checkAssignment(_, existingValues)).find(_.isDefined).flatten

private def checkUnique[Id](ids: Seq[Id], textRange: TextRange): Option[ErrorMessage] =
  if (ids.distinct.size != ids.size) 
    Option(ErrorMessage(s"repeated value declarations ${ids}", textRange))
  else 
    None

private def singleIds[T](p: LValueExpr[T]): Seq[SingleId[T]] =
  p match {
    case id @ SingleId(_, _, _) => Seq(id)
    case Untupling(parts, _) => parts.flatMap(singleIds)
    case _ => Seq()
  }

private def assignedIds[T](p: LValueExpr[T]): Seq[T] =
  singleIds(p).collect {
    case SingleId(id, _, AssignmentType.Assignment) => id 
  }

private def declaredVals[T](p: LValueExpr[T]): Seq[T] = 
  singleIds(p).collect {
    case SingleId(id, _, AssignmentType.Val) => id
  }