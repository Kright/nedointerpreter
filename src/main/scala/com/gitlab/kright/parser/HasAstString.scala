package com.gitlab.kright.parser
import com.gitlab.kright.interpreter.{UnaryOperatorName, stringify}
import com.gitlab.kright.util.quoted

trait HasAstString[T]:
  extension(t: T)
    def astString: String

given strAst: HasAstString[String] with
  extension(s: String)
    def astString: String = s

given decAst[T: HasAstString]: HasAstString[LValueExpr[T]] with
  extension (p: LValueExpr[T])
    def astString: String =
      p match
        case SingleId(id, _, assignmentType) => 
          assignmentType match {
            case AssignmentType.Assignment => id.astString
            case AssignmentType.Var => s"var ${id.astString}"
            case AssignmentType.Val => s"val ${id.astString}"
          }
        case Untupling(smth, _) => smth.map(_.astString).mkString("(", ", ", ")")
        case WildCard(_) => "_"
        case ExpectedValue(value, _) => value.stringify

given exprAst[T: HasAstString]: HasAstString[Expr[T]] with
  extension(e: Expr[T])
    def astString: String =
      e match
        case Expr.Binary(left, op, right) => s"{${left.astString} ${op.id.astString} ${right.astString}}"
        case Expr.Cortege(exprs, _) => exprs.map(_.astString).mkString("(", ", ", ")")
        case Expr.Block(exprs, _) => exprs.map(_.astString).mkString("{", "; ", "}")
        case Expr.Literal(value, _) =>
          value match
            case null => "null"
            case s: String => s.quoted
            case any => any.toString
        case Expr.Assignment(ids, value) => s"${ids.astString} = ${value.astString}"
        case Expr.Identifier(name, textRange) => name.astString
        case Expr.IfThenElse(condition, thenExpr, elseExpr, _) => s"if (${condition.astString}) ${thenExpr.astString}${elseExpr.map(e => s" else ${e.astString}").getOrElse("")}"
        case Expr.WhileLoop(condition, body, _) => s"while (${condition.astString}) ${body.astString}"
        case Expr.FuncCall(func, arg) => 
          func.astString match
            case UnaryOperatorName(name) => s"${name}${arg.astString}"
            case _ => s"${func.astString} ${arg.astString}"
