package com.gitlab.kright.parser

import com.gitlab.kright.scanner.{Lexeme, Token}
import com.gitlab.kright.util.TextRange
import com.gitlab.kright.util._
import com.gitlab.kright.interpreter.NedoValue
import com.gitlab.kright.interpreter.stringify
import com.gitlab.kright.optimizer.UniqueId

sealed trait LValueExpr[+Id]:
  def textRange: TextRange

enum AssignmentType:
  case Assignment, Val, Var

case class SingleId[+Id](id: Id, textRange: TextRange, assignmentType: AssignmentType) extends LValueExpr[Id]
case class Untupling[+Id](parts: Seq[LValueExpr[Id]], textRange: TextRange) extends LValueExpr[Id]
case class WildCard(textRange: TextRange) extends LValueExpr[Nothing]
case class ExpectedValue(value: NedoValue, textRange: TextRange) extends LValueExpr[Nothing]

object LValueExpr:
  def unapply(expr: Expr[String]): Option[LValueExpr[String]] =
    expr match
      case Expr.Identifier(name, textRange) =>
        if (name == "_")
          Option(WildCard(textRange))
        else
          Option(SingleId(name, textRange, AssignmentType.Assignment))
      case Expr.Literal(value, textRange) => Option(ExpectedValue(value, textRange))
      case Expr.Cortege(exprs, textRange) => exprs.map(unapply(_)).transposed.map(Untupling(_, textRange))
      case _ => None

  extension[T] (p: LValueExpr[T])
    def declaredIds: Seq[T] =
      p match 
        case SingleId(id, _, assignmentType) => 
          assignmentType match {
            case AssignmentType.Assignment => Seq()
            case AssignmentType.Var | AssignmentType.Val => Seq(id)
          }
        case Untupling(parts, _) => parts.flatMap(_.declaredIds)
        case WildCard(_) => Seq()
        case ExpectedValue(_, _) => Seq()