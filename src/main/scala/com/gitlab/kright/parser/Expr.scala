package com.gitlab.kright.parser

import com.gitlab.kright.scanner.{Line, Token}
import com.gitlab.kright.util.{HasTextRange, TextRange, quoted}
import com.gitlab.kright.interpreter.NedoValue
import com.gitlab.kright.interpreter.stringify

sealed abstract trait Expr[+Id] extends Product with HasTextRange

object Expr:
  case class Binary[+Id](left: Expr[Id], operator: Identifier[Id], right: Expr[Id]) extends Expr[Id]:
    override def textRange: TextRange = TextRange.inclusive(left.textRange, right.textRange)
  
  case class Cortege[+Id](exprs: Seq[Expr[Id]], textRange: TextRange) extends Expr[Id]
  
  case class Block[+Id](exprs: Seq[Expr[Id]], textRange: TextRange) extends Expr[Id]
  
  case class Assignment[+Id](ids: LValueExpr[Id], expr: Expr[Id]) extends Expr[Id]:
    override def textRange: TextRange = TextRange.inclusive(ids.textRange, expr.textRange)
  
  case class Identifier[+Id](id: Id, textRange: TextRange) extends Expr[Id]

  case class Literal(value: NedoValue, textRange: TextRange) extends Expr[Nothing]:
    override def toString: String = s"Literal(${value.stringify}, textRange)"

  case class IfThenElse[+Id](condition: Expr[Id], thenExpr: Expr[Id], elseExpr: Option[Expr[Id]], textRange: TextRange) extends Expr[Id]
  case class WhileLoop[+Id](condition: Expr[Id], body: Expr[Id], textRange: TextRange) extends Expr[Id]
      
  case class FuncCall[+Id](func: Expr[Id], arg: Expr[Id]) extends Expr[Id]:
    override def textRange: TextRange = TextRange.inclusive(func.textRange, arg.textRange)

  def transform[T](expr: Expr[T], f: Expr[T] => Expr[T]): Expr[T] =
    expr match {
      case Binary(left, operator, right) => f(Binary(transform(left, f), operator, transform(right, f)))
      case c @ Cortege(exprs, textRange) => f(c.copy(exprs = exprs.map(transform(_, f))))
      case b @ Block(exprs, textRange) =>   f(b.copy(exprs = exprs.map(transform(_, f))))
      case a @ Assignment(ids, expr) => f(a.copy(expr = transform(expr, f)))
      case i @ Identifier(id, textRange) => f(i)
      case l @ Literal(value, textRange) => f(l)
      case IfThenElse(condition, thenExpr, elseExpr, textRange) => 
        f(IfThenElse(transform(condition, f), transform(thenExpr, f), elseExpr.map(transform(_, f)), textRange))
      case WhileLoop(condition, body, textRange) => 
        f(WhileLoop(transform(condition, f), transform(body, f), textRange))
      case FuncCall(func, arg) => f(FuncCall(transform(func, f), transform(arg, f)))
    }
        
  extension[Id] (e: Expr[Id])
    def blockDeclaredIds: Seq[Id] =
      e match
        case Expr.Block(exprs, _) => exprs.flatMap(_.declaredIds)
        case _ => Seq()
      
    def declaredIds: Seq[Id] =
      e match 
        case Expr.Assignment(ids, _) => ids.declaredIds
        case _ => Seq()
    
    def accessedIds: Set[Id] =
      e match 
        case Expr.Binary(left, op, right) => left.accessedIds ++ right.accessedIds + op.id
        case Expr.Cortege(exprs, _) => exprs.flatMap(_.accessedIds).toSet
        case Expr.Block(exprs, _) => exprs.flatMap(_.accessedIds).toSet
        case l: Expr.Literal => Set()
        case Expr.Assignment(ids, value) => ids.declaredIds.toSet ++ value.accessedIds
        case Expr.Identifier(id, _) => Set(id)
        case Expr.IfThenElse(condition, thenExpr, elseExpr, _) => 
          condition.accessedIds ++ thenExpr.accessedIds ++ elseExpr.map(_.accessedIds).getOrElse(Set())
        case Expr.WhileLoop(condition, body, _) => condition.accessedIds ++ body.accessedIds
        case Expr.FuncCall(func, arg) => func.accessedIds ++ arg.accessedIds
