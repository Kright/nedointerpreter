package com.gitlab.kright.parser

import com.gitlab.kright.optimizer.UniqueId

trait HasName[T]:
  extension (t: T)
    def name: String

given stringIdName: HasName[String] with
  extension (s: String)
    def name: String = s

given uniqueIdName: HasName[UniqueId] with
  extension (id: UniqueId)
    def name: String = id.name