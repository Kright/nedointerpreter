package com.gitlab.kright.parser

import java.beans.Expression
import scala.util.chaining._
import com.gitlab.kright.ErrorMessage
import com.gitlab.kright.scanner.Token
import com.gitlab.kright.scanner.TokenType
import com.gitlab.kright.interpreter.{NedoValue, UnaryOperatorName, UnaryOperators}
import com.gitlab.kright.parser.Expr.{Identifier, IfThenElse, Literal}
import com.gitlab.kright.util.TextRange

import scala.collection.mutable.ArrayBuffer

// todo write abstract parser
private class Parser(tokens: IndexedSeq[Token]):
  require(tokens.lastOption.exists(_.tokenType == TokenType.Eof), "last token should be OEF")
  
  import Parser.ParserResult

  private var currentPos = 0

  private def peek(offset: Int = 0): Option[Token] = 
    tokens.lift(currentPos + offset)
    
  private def isAtEnd(): Boolean =
    peek(0).get.tokenType == TokenType.Eof

  private def advance(): Option[Token] = 
    peek(0).tap { _ => 
      if (!isAtEnd()) currentPos += 1
    }
    
  private def consume(tokenType: TokenType, errorMsg: String): ParserResult[Token] =
    val current = peek().get
    if (current.tokenType == tokenType)
      Right(advance().get)
    else
      Left(ErrorMessage(errorMsg, current))

  private def tokenMatch(tokenType: TokenType*): Boolean =
    tokenType.find(checkToken).map{ _ =>
      advance()
      true
    }.getOrElse(false)

  private def checkToken(tokenType: TokenType): Boolean =
    peek().exists(_.tokenType == tokenType)
  
  private def leftAssociativeBinop(parseUnderlying: () => ParserResult[Expr[String]], operators: Seq[TokenType]): ParserResult[Expr[String]] =
    var expr: Expr[String] = parseUnderlying() match
      case Left(error) => return Left(error)
      case Right(e) => e 
    
    while(tokenMatch(operators: _*))
      val operator = peek(-1).get
      
      val right = parseUnderlying() match
        case Left(errorMessage) => return Left(ErrorMessage(s"cannot parse ${expr} ${operator} _", errorMessage))
        case Right(e) => e
      expr = Expr.Binary(expr, operator.pipe(t => Expr.Identifier(t.lexeme.text, t.textRange)), right)
    Right(expr)

  private def parseSeq[T](parse: () => ParserResult[T], shouldStop: () => Boolean): ParserResult[Seq[T]] =
    val arr = new ArrayBuffer[T]()
    while (!shouldStop()) {
      parse() match
        case Left(error) => return Left(error)
        case Right(t) => arr += t
    }
    Right(arr.toSeq) 
  
  private def parseSepSeq[T](parse: () => ParserResult[T], consumeSep: () => Boolean, shouldStop: () => Boolean): ParserResult[Seq[T]] =
    val arr = new ArrayBuffer[T]()
    while (!shouldStop()) {
      parse() match
        case Left(error) => return Left(error)
        case Right(t) => 
          arr += t
          if (!consumeSep()) {
            return Right(arr.toSeq)
          }
    }
    Right(arr.toSeq)

  // hand-write parsing
  
  def apply() = program()
  
  private def program(): ParserResult[Expr.Block[String]] = 
    parseSepSeq(statement, 
                () => consume(TokenType.Semicolon, "expected ';' between expressions").isRight,
                () => isAtEnd()).flatMap(seq => 
      if (isAtEnd()) 
        Right(Expr.Block(seq, TextRange.inclusive((seq.map(_.textRange) :+ TextRange(0, 0)) :_*)))
      else
        Left(ErrorMessage("can't parse program to the end", {seq.lastOption.map(_.textRange).getOrElse(TextRange(0, 0))}))
    )
  
  private def block(): ParserResult[Expr.Block[String]] = 
    for {
      leftBrance <- consume(TokenType.LeftBrace, "expect '{' at start of block")
      statements <- parseSepSeq(statement, 
                                () => consume(TokenType.Semicolon, "expected ';' between expressions").isRight, 
                                () => peek(0).exists(_.tokenType == TokenType.RightBrace))
      rightBrace <- consume(TokenType.RightBrace, "expect '}' at the end of block")
    } yield Expr.Block(statements, TextRange.inclusive(leftBrance.textRange, rightBrace.textRange))
  
  private def tuple(): ParserResult[Expr.Cortege[String]] =
    for {
      leftParen <- consume(TokenType.LeftParen, "expect '(' at start of tuple")
      exprs <- parseSepSeq(expression, () => consume(TokenType.Comma, "").isRight, () => peek(0).exists(_.tokenType == TokenType.RightParen))
      rightParen <- consume(TokenType.RightParen, "expect ')' at the end of tuple")
    } yield Expr.Cortege(exprs, TextRange.inclusive(leftParen.textRange, rightParen.textRange))
  
  private def statement(): ParserResult[Expr[String]] =
    if (tokenMatch(TokenType.Var)) return varDeclaration(isMutable = true)
    if (tokenMatch(TokenType.Val)) return varDeclaration(isMutable = false)
    return expression()
  
  private def varDeclaration(isMutable: Boolean): ParserResult[Expr.Assignment[String]] =
    for {
      ids <- deconstructionPattern(isMutable)
      _ <- consume(TokenType.Equal, "expected '=' in variable declaration")
      value <- expression()
    } yield Expr.Assignment(ids, value)
    
  private def deconstructionPattern(isMutable: Boolean): ParserResult[LValueExpr[String]] =
    if (peek().exists(_.tokenType == TokenType.LeftParen)) 
      return for {
        leftParen <- consume(TokenType.LeftParen, "expect '(' at start of tuple")
        idsOrTuples <- parseSepSeq(() => deconstructionPattern(isMutable), () => consume(TokenType.Comma, "").isRight, () => peek(0).exists(_.tokenType == TokenType.RightParen))
        rightParen <- consume(TokenType.RightParen, "expect ')' at the end of tuple")
      } yield Untupling(idsOrTuples, TextRange.inclusive(leftParen.textRange, rightParen.textRange))
      
    if (tokenMatch(TokenType.True)) 
      return Right(ExpectedValue(true, peek().get.textRange))
    
    if (tokenMatch(TokenType.False)) 
      return Right(ExpectedValue(false, peek().get.textRange))
    
    if (tokenMatch(TokenType.Nil))
      return Right(ExpectedValue(null, peek().get.textRange))

    if (tokenMatch(TokenType.Var)) {
      return deconstructionPattern(true)
    }
    
    if (tokenMatch(TokenType.Val)) {
      return deconstructionPattern(false)
    }
    
    val top = peek()
    if (tokenMatch(TokenType.Number, TokenType.LexString))
      return Right(ExpectedValue(top.get.literalObject.get, top.get.textRange))

    return consume(TokenType.Identifier, "expect identifier after 'var' keyword").map{ id =>
      id.lexeme.text match
        case "_" => WildCard(id.textRange)
        case _ => SingleId(id.lexeme.text, id.textRange,
          if (isMutable) AssignmentType.Var else AssignmentType.Val)
    }
    
  private def ifExpression(): ParserResult[Expr.IfThenElse[String]] = 
    val ifThenElse = for {
      ifTok <- consume(TokenType.If, "expected 'if' at the begining of conditional if")
      _ <- consume(TokenType.LeftParen, "expected '(' before condition expression")
      condExpr <- expression()
      _ <- consume(TokenType.RightParen, "expected ')' after condition expression")
      thenExpr <- expression()
    } yield IfThenElse(condExpr, thenExpr, None, TextRange.inclusive(ifTok.textRange, thenExpr.textRange))
    
    if (peek(0).exists(_.tokenType == TokenType.Else))
      for {
        ifteExpr <- ifThenElse
        _ <- consume(TokenType.Else, "expected 'else' in if block")
        eExpr <- expression()
      } yield ifteExpr.copy(
        elseExpr = Option(eExpr), 
        textRange = TextRange.inclusive(ifteExpr.textRange, eExpr.textRange)
      )
    else 
      ifThenElse
      
  private def whileDoLoop(): ParserResult[Expr.WhileLoop[String]] = 
    for {
      whileWord <- consume(TokenType.While, "expected 'while' at start of while loop")
      _ <- consume(TokenType.LeftParen, "expected '(' at start of while loop")
      condition <- expression()
      _ <- consume(TokenType.RightParen, "expected ')' after condition of while loop")
      body <- expression()
    } yield Expr.WhileLoop(condition, body, TextRange.inclusive(whileWord.textRange, body.textRange))
  
  private def expression(): ParserResult[Expr[String]] =
    if (peek().exists(_.tokenType == TokenType.If)) return ifExpression()
    if (peek().exists(_.tokenType == TokenType.While)) return whileDoLoop()
    assignment()
  
  private def assignment(): ParserResult[Expr[String]] = 
    logicOr().flatMap { left =>
      if (tokenMatch(TokenType.Equal)) 
        equality().flatMap{ right => 
          left match {
            case LValueExpr(pattern) => Right(Expr.Assignment(pattern, right))
            case unexpected => Left(ErrorMessage(s"lvalue of assignment should be id, get ${unexpected}", left))
          }
        }
      else 
        Right(left)
    }
    
  private def logicOr(): ParserResult[Expr[String]] = 
    leftAssociativeBinop(logicAnd, Seq(TokenType.Or))

  private def logicAnd(): ParserResult[Expr[String]] = 
    leftAssociativeBinop(equality, Seq(TokenType.And))

  private def equality(): ParserResult[Expr[String]] = 
    leftAssociativeBinop(comparison, Seq(TokenType.BangEqual, TokenType.EqualEqual))
  
  private def comparison(): ParserResult[Expr[String]] = 
    leftAssociativeBinop(term, Seq(TokenType.Greater, TokenType.GreaterEqual, TokenType.Less, TokenType.LessEqual))

  private def term(): ParserResult[Expr[String]] = 
    leftAssociativeBinop(factor, Seq(TokenType.Minus, TokenType.Plus))
    
  private def factor(): ParserResult[Expr[String]] =
    leftAssociativeBinop(unary, Seq(TokenType.Slash, TokenType.Star))
  
  private def unary(): ParserResult[Expr[String]] = 
    if (tokenMatch(TokenType.Bang, TokenType.Minus))
      val op = peek(-1).get
      val right = unary() match 
        case Left(errorMessage) => return Left(ErrorMessage(s"can't parse unary: ${op} _", errorMessage))
        case Right(e) => e
      Right(Expr.FuncCall(op.pipe(t => Expr.Identifier(UnaryOperatorName(t.lexeme.text), t.textRange)), right))
    else
      funcCalls()
      
  private def funcCalls(): ParserResult[Expr[String]] = 
    primary() match 
      case err @ Left(errorMessage) => err 
      case Right(firstExpr) => funcCalls(firstExpr)
  
  private def funcCalls(func: Expr[String]): ParserResult[Expr[String]] = 
    primary() match 
      case Left(_) => Right(func)
      case Right(args) => funcCalls(Expr.FuncCall(func, args))
  
  private def primary(): ParserResult[Expr[String]] =
    def literal(obj: NedoValue, token: Token): ParserResult[Expr[String]] = 
      Right(Expr.Literal(obj, token.lexeme.textRange))

    if (tokenMatch(TokenType.False)) return literal(false, peek(-1).get)
    if (tokenMatch(TokenType.True)) return literal(true, peek(-1).get)
    if (tokenMatch(TokenType.Nil)) return literal(null, peek(-1).get)
    if (tokenMatch(TokenType.Number, TokenType.LexString)) return literal(peek(-1).get.literalObject.get, peek(-1).get)
    if (tokenMatch(TokenType.Identifier)) return Right(peek(-1).get.pipe(t => Expr.Identifier(t.lexeme.text, t.textRange)))

    val currentToken = peek()
   
    if (currentToken.get.tokenType == TokenType.LeftParen) 
      return tuple()
    
    if (currentToken.get.tokenType == TokenType.LeftBrace) 
      return block() 
      
    Left(ErrorMessage("Can't parse primary", currentToken.get))

  private def synchronize(): Unit = 
    advance()
    
    while !isAtEnd() do
      if (peek(-1).exists(_.tokenType == TokenType.Semicolon))
        return 
        
      peek(0).get.tokenType match 
        case TokenType.Class | TokenType.Def | TokenType.Var
             | TokenType.For  | TokenType.If  | TokenType.While  
             | TokenType.Return => return
        case _ => 
      
      advance()


object Parser:
  type ParserResult[T] = Either[ErrorMessage, T]
  
  def apply(tokens: IndexedSeq[Token]): ParserResult[Expr[String]] = 
    new Parser(tokens)()