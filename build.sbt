val dottyVersion = "3.0.2"

lazy val root = project
  .in(file("."))
  .settings(
    name := "nedo interpreter",
    version := "0.1.0",

    scalaVersion := dottyVersion,

    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test",
    
    scalacOptions ++= Seq(
      "-Xfatal-warnings",
    )
  )
