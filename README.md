## code interpreter

I was inspired by awesome book [Crafting Interpreters](https://craftinginterpreters.com). 
This is my attempt to implement interpreter in Scala for very simple language.
Work in progress now.
 
### How to run

This is a normal sbt project, you can compile code with `sbt compile` and run it
with `sbt run`, `sbt console` will start a Dotty REPL.

For more information on the sbt-dotty plugin, see the
[dotty-example-project](https://github.com/lampepfl/dotty-example-project/blob/master/README.md).
